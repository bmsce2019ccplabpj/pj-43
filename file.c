#include<stdio.h>

int main()
{
    char c;
    FILE *f;
    f=fopen("INPUT.txt","w");
    printf("\nEnter ctrl+D to exit");
    printf("\nEnter the data in the given file ");
    while((c=getchar())!=EOF)
    {
        fputc(c,f);
    }
    fclose(f);
    f=fopen("INPUT.txt","r");
    printf("\nThe contents of the file are ");
    while((c=fgetc(f))!=EOF)
    {
        putchar(c);
    }
    fclose(f);
    return 0;
}