
#include<stdio.h>
void input(int *,int *);
void convert(int *,int *,int *);
void output(int *);

int main()
{
    int a,b,c;
    input(&a,&b);
    convert(&a,&b,&c);
    output(&c);
    return 0;
}

void input(int *p,int *q)
{
    printf("enter the time in hours\n");
    scanf("%d",p);
    printf("enter the time in minutes\n");
    scanf("%d",q);
}

void convert(int *l,int *m,int *n)
{
    *n=(*l*60)+*m;
}

void output(int *z)
{
    printf("Time in minutes=%d\n",*z);
}