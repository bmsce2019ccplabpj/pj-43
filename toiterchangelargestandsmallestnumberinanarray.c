#include<stdio.h>
int main()
{
   int n,i,a[10],smallest,largest,pos_smallest=0,pos_largest=0,temp;
   printf("Enter the number of elements in an array\n");
   scanf("%d",&n);
   printf("Enter the elements");
for(i=0;i<n;i++)
 {
   scanf("%d",&a[i]);
 }
for(i=0;i<n;i++)
 {
   printf("%d ",a[i]);
 }
smallest=a[0];

for(i=1;i<n;i++)
 {
   if(a[i]<smallest)
   {
   smallest=a[i];
   pos_smallest=i;
   }
 }
largest=a[0];
for(i=1;i<n;i++)
 {
  if(a[i]>largest)
  {
   largest=a[i];
   pos_largest=i;
   }
 }
printf("\nBEFORE INTERCHANGING");
printf("\nThe smallest number in the given array is %d and its position is %d",smallest,pos_smallest);
printf("\nThe largest number in the given array is %d and its position is %d",largest,pos_largest);
temp=a[pos_largest];
a[pos_largest]=a[pos_smallest];
a[pos_smallest]=temp;
printf("\nAFTER INTERCHANGING");
printf("\nThe smallest number is=%d",a[pos_smallest]);
printf("\nThe largest number is=%d",a[pos_largest]);

printf("\nThe new array is");
for(i=0;i<n;i++)
 {
  printf("%d ",a[i]);
 }
return 0;
}