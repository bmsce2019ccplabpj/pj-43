#include<stdio.h>
void input(int *p,int *q)
{
 printf("\nEnter any two integers");
 scanf("%d%d",p,q);
}
void swap(int *x,int *y)
{
 *x=*x+*y;
 *y=*x-*y;
 *x=*x-*y;
}
void output(int x1,int x2)
{
 printf("\nAfter swapping numbers a=%d b=%d",x1,x2);
}
void main()
{
 int a,b;
 input(&a,&b);
 swap(&a,&b);
 output(a,b);
}