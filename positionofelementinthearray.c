#include<stdio.h>

int find_pos(int n,int a[n],int element)
{
int i,position=-1;
   for(i=0;i<n;i++)
   { 
       {
       if(a[i]==element)
       position=i;
       }   
   }
return position;
}

int main()
{
int n,a[10],x,element;
printf("Enter the number of elements in an array\n");
scanf("%d",&n);

for(int i=0;i<n;i++)
   {
   printf("Enter the elements\n");
   scanf("%d",&a[i]);
   }
printf("Enter the element whose position is to be found\n");
scanf("%d",&element);

x=find_pos(n,a,element);

if(x==-1)
{
    printf("The position of element is not found");
}
else
{
    printf("The position of the element =%d",x);
}

return 0;
}
